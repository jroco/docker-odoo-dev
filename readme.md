# Odoo docker image for development
## Installation

### Requirements
* Docker
* Docker-Compose

## Usage
Clone this repository to your laptop and run:
```bash
docker-compose up -d
```

Then access to localhost:5000

# Addons
You can add your addons on the odoo/addons Folder

# Run Commands inside Odoo container
```bash
docker exec -it odoodev_odoo_1 bash
```
# Odoo configuration files

There is a minimal odoo.conf file in odoo/config folder.


If you need multiple instances running at the same time, duplicate the folder and change the port 5000 to another one in the file docker-compose.yml
